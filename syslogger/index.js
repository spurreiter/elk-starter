const os = require('os')
const syslog = require('syslog-client')

const options = {
  syslogHostname: os.hostname(),
  // transport: syslog.Transport.Udp,
  // port: 1514
  transport: syslog.Transport.Tcp,
  port: 1515
}
const client = syslog.createClient('127.0.0.1', options)

client.log('this is a plain message')

client.log('{"level":ERROR, "message": "BROKEN Json message"}')

client.log(JSON.stringify({
  level: 'DEBUG',
  message: 'ALL OK this is a json message'
}))

client.log(JSON.stringify({
  level: 'INFO',
  message: 'WITH NUMBER this is a json message',
  app: 'test',
  test: 100
}))

client.log(JSON.stringify({
  level: 'ERROR',
  message: 'DEAD this is a json message',
  app: 'test',
  test: 'astringnow10'
}))

client.log(JSON.stringify({
  level: 'INFO',
  http: {
    url: '/home/user/documents/folder/file?query=1&foo=bar',
    method: 'GET',
    status: 200
  }
}))

client.log(JSON.stringify({
  level: 'WARN',
  http: {
    url: '/not-found',
    method: 'GET',
    status: 404,
    headers: {
      'user-agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:97.0) Gecko/20100101 Firefox/97.0'
    }
  }
}))

client.log(JSON.stringify({
  level: 'ERROR',
  http: {
    url: '/not-found',
    method: 'GET',
    status: 404,
    headers: {
      'user-agent': 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:97.0) Gecko/20100101 Firefox/97.0'
    }
  }
}))
