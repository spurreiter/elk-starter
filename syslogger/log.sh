#!/bin/bash

# send log lines via syslog

HOST=127.0.0.1
PORT=1514

_syslog () {
	local message="$@"
	echo -n "<150>`env LANG=us_US.UTF-8 date "+%b %d %H:%M:%S"` $HOSTNAME $message" \
		| nc -v -u -w1 "$HOST" "$PORT"
}

_syslog "this is NOT json"
_syslog '{"level":"INFO","message":"this is json"}'
