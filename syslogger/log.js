const os = require('os')
const syslog = require('syslog-client')

const help = () => {
  console.log(`
    node log.js [options] message

    -h|--help   this help
    --udp       use udp (port:1514) instead of tcp (port:1515)

    example:

    node log.js --udp '{"level": "INFO", "message": "Hello World"}'
  `)
  process.exit()
}

function argv (args) {
  const argv = args || process.argv.slice(2)
  const cmd = { files: [] }

  while (argv.length) {
    const arg = argv.shift()

    switch (arg) {
      case '-h':
      case '--help':
        help()
        break
      case '--udp':
        cmd.udp = true
        break
      default:
        cmd.message = arg
    }
  }

  if (!cmd.message) {
    help()
  }

  return cmd
}

const main = () => {
  const cmd = argv()

  const options = {
    syslogHostname: os.hostname(),
    transport: cmd.udp ? syslog.Transport.Udp : syslog.Transport.Tcp,
    port: cmd.udp ? 1514 : 1515
  }

  const client = syslog.createClient('127.0.0.1', options)

  client.log(cmd.message)
}

main()
