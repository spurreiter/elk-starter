#!/bin/sh

# check connection to elasticsearch node
# needs curl and jq installed

CWD=$(cd -P -- "$(dirname -- "$0")" && pwd -P)

. "$CWD/../.env"

curl -v -u "$ELASTIC_USER:$ELASTIC_PASS" "$ELASTIC_HOST" | jq
