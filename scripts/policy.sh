#!/bin/bash

# set -x

# setup s1 index strategy
# one index with rollover strategy with alias
# needs curl, jq

CWD=$(cd -P -- "$(dirname -- "$0")" && pwd -P)

. $CWD/../.env

_curl () {
	curl -v -H "content-type: application/json" -u "$ELASTIC_USER:$ELASTIC_PASS" "$@"
}

_policyname () {
	echo -n "ilm-logs-${1}"
}

_get_ilm_policy () {
	local policy=$(_policyname $POLICY)
	_curl "$ELASTIC_HOST/_ilm/policy/$policy" | jq .
}

# https://www.elastic.co/guide/en/elasticsearch/reference/master/ilm-put-lifecycle.html
_put_ilm_policy () {
	local policy=$(_policyname $POLICY)
	local data=$(cat <<-EOS
	{
		"policy": {
			"phases": {
				"hot": {
					"actions": {
						"rollover": {
							"max_primary_shard_size": "1GB",
							"max_age": "30d"
						}
					}
				}
			}
		}
	}

	EOS
	)
	_curl "$ELASTIC_HOST/_ilm/policy/$policy" -X PUT --data "$data" | jq
}

_delete_ilm_policy () {
	local policy=$(_policyname $POLICY)
	_curl "$ELASTIC_HOST/_ilm/policy/$policy" -X DELETE | jq .
}

_help () {
	cat <<-EOS

	  policy.sh <policy> [OPTIONS]

	  <policy>     index policy name

	  -h|--help    this help
	  --create     create index policy with <policy>
	  --delete     delete index policy with <policy>
	EOS
}

#----

case $1 in
--help|-h)
	_help
	exit
	;;
esac

POLICY="$1"

if [ -z "$POLICY" ]; then
	echo "Error: Need a policy name"
	_help
	exit 1
fi

case $2 in
--create)
	_put_ilm_policy $POLICY
	;;
--delete)
	_delete_ilm_policy $POLICY
	;;
esac

_get_ilm_policy $POLICY
