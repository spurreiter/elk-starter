#!/usr/bin/env node

const { randomUUID } = require('crypto')

const ELASTIC_HOST = 'http://elasticsearch:9200'

const readStdin = () => {
  let data = ''
  const { stdin } = process
  stdin.setEncoding('utf8')
  return new Promise((resolve, reject) => {
    stdin.on('data', chunk => { data += chunk.toString() })
    stdin.on('error', (err) => { reject(err) })
    stdin.on('end', () => { resolve(data) })
  })
}

const parsePasswords = (data) => {
  const RE = /^PASSWORD (\S+) = (\S+)\s*$/
  const codes = {}
  for (let line of data.split(/\n/)) {
    const m = RE.exec(line)
    if (m) {
      const [_, user, pass] = m
      codes[user] = pass
    }
  }
  return codes
}

const generateEnv = (codes) => {
  const out = [
    `ELASTIC_HOST=${ELASTIC_HOST}`,
    `ENC_KEY_1=${randomUUID()}`,
    `ENC_KEY_2=${randomUUID()}`,
    `ENC_KEY_3=${randomUUID()}`,
    ''
  ]

  for (let [user, pass] of Object.entries(codes)) {
    const ucUser = `${user}_USER`.toUpperCase()
    const ucPass = `${user}_PASS`.toUpperCase()
    out.push(`${ucUser}=${user}`)
    out.push(`${ucPass}=${pass}`)
    out.push('')
  }
  return out.join('\n')
}

const main = async () => {
  const data = await readStdin()
  const codes = parsePasswords(data)
  const out = generateEnv(codes)
  console.log(out)
}

main().catch(console.error)
