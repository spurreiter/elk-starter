#!/bin/bash

# script to start logstash container standalone
# comment-out the logstash service in docker-compose.yml

CWD=$(cd -P -- "$(dirname -- "$0")" && pwd -P)

. "$CWD/../.env"

_volume_create () {
	docker volume rm "$1"
	docker volume create \
		--driver local \
    --opt type=tmpfs \
    --opt device=tmpfs \
    --opt o=uid=1000 \
	"$1"
}

_volume_create logstash_data

ARGS=@$
if [ -z "$1" ]; then
	ARGS=bash
	echo INFO: Start with "logstash"
fi

docker run \
	-it --rm --name logstash \
	--add-host=elasticsearch:127.0.0.1 \
	--network=host \
	-e LS_SETTINGS_DIR=/etc/logstash \
	-e LOGSTASH_SYSTEM_PASS="$LOGSTASH_SYSTEM_PASS" \
	-e ELASTIC_USER="$ELASTIC_USER" \
	-e ELASTIC_PASS="$ELASTIC_PASS" \
	-p "1514:1514" \
	-p "1515:1515" \
	-p "5044:5044" \
	-v "$CWD/../logstash:/etc/logstash" \
	-v "logstash_data:/var/local" \
	logstash:8.0.0 \
	$ARGS

	# -u root \
	#logstash --path.settings /etc/logstash
