#!/bin/bash

# set -x

# setup s2 index strategy
# one index by env, service with rollover strategy with alias
#
# needs curl, jq
#
# in _put_ilm_template edit all number or boolean values under
# `template.mappings.properties`

CWD=$(cd -P -- "$(dirname -- "$0")" && pwd -P)

. $CWD/../.env

_curl () {
	curl -v -H "content-type: application/json" -u "$ELASTIC_USER:$ELASTIC_PASS" "$@"
}

_indexname () {
	echo -n "ilm-logs-${1}"
}
_indexpattern () {
	echo -n "logs-${1}-*"
}

_get_ilm_template () {
	local indexname=$(_indexname "$1")
	_curl "$ELASTIC_HOST/_index_template/$indexname" | jq .
}

_delete_ilm_template () {
	local indexname=$(_indexname "$1")
	_curl "$ELASTIC_HOST/_index_template/$indexname" -X DELETE | jq .
}

# https://www.elastic.co/guide/en/elasticsearch/reference/master/index-templates.html
_put_ilm_template () {
	local indexpattern=$(_indexpattern "$1")
	local indexname=$(_indexname "$1")
	local data=$(cat <<-EOS
	{
		"index_patterns": [
			"$indexpattern"
		],
		"template": {
			"settings": {
				"index.lifecycle.name": "$indexname"
			},
			"mappings": {
				"dynamic": true,
				"numeric_detection": false,
				"date_detection": true,
				"dynamic_date_formats": [
					"strict_date_optional_time",
					"yyyy/MM/dd HH:mm:ss Z||yyyy/MM/dd Z"
				],
				"_source": {
					"enabled": true,
					"includes": [],
					"excludes": []
				},
				"_routing": {
					"required": false
				},
				"dynamic_templates": [],
				"properties": {
					"message": { "type": "text" },
					"timestamp": { "type": "date" },
					"log": { "type": "object" },
					"log.level": { "type": "text" },
					"log.name": { "type": "text" },
					"client": { "type": "object" },
					"client.ip": { "type": "ip" },
					"client.port": { "type": "long" },
					"error": { "type": "object" },
					"error.message": { "type": "text" },
					"error.stack_trace": { "type": "text" },
					"host": { "type": "object" },
					"http": { "type": "object" },
					"http.method": { "type": "text" },
					"http.request": { "type": "object" },
					"http.request.id": { "type": "text" },
					"http.request.bytes": { "type": "long" },
					"http.request.headers": { "type": "object" },
					"http.request.headers.user-agent": { "type": "text" },
					"http.request.headers.x-request-id": { "type": "text" },
					"http.response": { "type": "object" },
					"http.response.bytes": { "type": "long" },
					"http.response.ms": { "type": "long" },
					"http.response.status_code": { "type": "integer" },
					"http.response.headers": { "type": "object" },
					"labels": { "type": "object" },
					"labels.app": { "type": "text" },
					"service": { "type": "object" },
					"service.environment": { "type": "text" }
				}
			}
		}
	}

	EOS
	)
	_curl "$ELASTIC_HOST/_index_template/$indexname" -X PUT --data "$data" | jq
}

_help () {
	cat <<-EOS

	  template.sh <template> [OPTIONS]

	  <template>   template name

	  -h|--help    this help
	  --create     create template <template>
	  --delete     delete template <template>
	EOS
}

#----

case $1 in
--help|-h)
	_help
	exit
	;;
esac

TEMPLATE="$1"

if [ -z "$TEMPLATE" ]; then
	echo "Error: Need a template name"
	_help
	exit 1
fi

case $2 in
--create)
	_put_ilm_template $TEMPLATE
	;;
--delete)
	_delete_ilm_template $TEMPLATE
	;;
esac

_get_ilm_template $TEMPLATE

