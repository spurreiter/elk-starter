
# syslog

https://datatracker.ietf.org/doc/html/rfc3164#section-4.1.1

```
priority = facility * 8 + severity
# e.g. for "local use 4" with severity "Notice"
165 = 20 * 8 + 5
```

Send syslog messages via netcat
```
echo "<150>`env LANG=us_US.UTF-8 date "+%b %d %H:%M:%S"` hostname my special message goes here"
| nc -v -u -w 0 _syslog_host_ 514

<150>Jan 24 21:12:31 hostname my special message goes here
```

# grok

https://logz.io/blog/logstash-grok/

A great way to get started with building your grok filters is this grok debug tool: https://grokdebug.herokuapp.com/

[Logstash Grok Patterns](https://github.com/logstash-plugins/logstash-patterns-core/tree/master/patterns)
