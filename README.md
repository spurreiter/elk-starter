# Elasticsearch-Kibana-Logstash Starter

> Docker compose for easy start with ELK

**Table of Contents**

<!-- !toc -->

* [Requirements](#requirements)
* [Overview](#overview)
* [Startup](#startup)
* [Setup index strategy](#setup-index-strategy)
* [Create some log-entries](#create-some-log-entries)
* [Shutdown and remove all](#shutdown-and-remove-all)
* [Developing logstash](#developing-logstash)
* [License](#license)
* [Contributing](#contributing)
* [References](#references)

<!-- toc! -->

# Requirements

Make sure you have `curl`, `jq` and `node` installed on your system.

# Overview

```
     o
     X
    User
     |
     | http:5601
     V
  +------+
  |Kibana|
  +------+
     |
     |
     V
  +-------+     +--------+  udp:1514
  |Elastic|<----|Logstash|<-------------[syslog]
  +-------+     +--------+  tcp:1515
                     A
                     | :5044
                     |
                  [beats]
```

# Startup

```bash
# create a hosts entry
sudo vi /etc/hosts
127.0.0.1   elasticsearch
# startup Kibana + Elastic
docker-compose up
# Generate a password for elastic
docker exec -it elasticsearch bin/elasticsearch-setup-passwords \
    auto --batch --url http://elasticsearch:9200 | tee vars.log
# convert vars.log into .env
cat vars.log | node ./scripts/env.js > .env
```

Now shutdown the stack with CTRL+C restart the stack again to start with the
updated environment variables.

Login as user `elastic` to kibana at http://0.0.0.0:5601

# Setup index strategy

For each environment dev, test, prod a different index is set up

```bash
# create policy first e.g. env=dev
./scripts/policy.sh dev --create
# create index template
# Here you can define the objects and their indexing type in Elastic
# See `_put_ilm_template()`
./scripts/template.sh dev --create
```

create a data-view with the name `logs-*-*` at 
[Management > Stack Management > Data views](http://localhost:5601/app/management/kibana/dataViews)

Then access [Analysis > Discover](localhost:5601/app/discover) and create a search.

# Create some log-entries

```bash
cd syslogger
./log.sh
node index.js
```

# Shutdown and remove all

```bash
# tear down stack
docker-compose down
# delete the volume (if you like)
docker volume rm elk-starter_esdata elk-starter_logstashdata
```

# Developing logstash

```
logstash
├── logstash.yml
├── pipelines
│   ├── input
│   │   ├── 01-input-beats.conf
│   │   ├── 02-input-syslog.conf
│   │   ├── 50-filter-syslog.conf
│   │   ├── 51-filter-env.conf
│   │   └── 99-output-sendto.conf
│   ├── json
│   │   ├── 00-input-address.conf
│   │   ├── 50-filter.conf
│   │   └── 99-output-sendto.conf
│   ├── deadletter
│   │   ├── 01-input-deadletter.conf
│   │   ├── 50-filter-prune.conf
│   │   └── 99-output-sendto.conf
│   └── output
│       ├── 00-input-address.conf
│       ├── 50-url-level.conf
│       ├── 90-output-debug.conf
│       └── 91-output-elasticsearch.conf
└── pipelines.yml
```

The logstash configuration uses multiple pipelines.

An application shall log in [NDJSON](http://ndjson.org/) using the
[Elastic Common Schema][].

To allow for greater simplicity the following mappings are performed in the
filter pipelines. See `./logstash/pipelines/json/50-filter.conf` for the details.

All messages which are not accepted by elastic (usually an indexing conflict) are 
later processed in the 
[deadletter queue](https://www.elastic.co/guide/en/logstash/current/dead-letter-queues.html).

Any log line that can't be JSON parsed of have passed the deadletter queue will
be attributed with `log.level=REVIEW` to allow for later correction in the
application.

Any changes to the logstash config are automatically reloaded.

See `./logstash/logstash.yml`

```yaml
# ...snip...
config.reload.automatic: true
```

For production use set this line to `false`.

# License

All code is and configuration is licensed under [The Unlicense](https://unlicense.org/).

# Contributing

Contributions are welcome. Fork the project, modify it and create a Merge-Request...

# References

- [Elastic Common Schema][]
- [ECS Field Reference](https://github.com/elastic/ecs/blob/8.0/generated/csv/fields.csv)
- [logstash Grok Pattern](https://github.com/logstash-plugins/logstash-patterns-core/blob/main/patterns/ecs-v1/grok-patterns)
- [ELK index strategies](https://dev.to/kreuzwerker/exploring-logging-strategies-with-the-elastic-stack-22jn)
- https://www.elastic.co/blog/managing-time-based-indices-efficiently
- https://logz.io/blog/managing-elasticsearch-indices/

[Elastic Common Schema]: https://www.elastic.co/guide/en/ecs/current/index.html
